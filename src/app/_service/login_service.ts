import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseBase } from '../_model/responseBase';
import { User } from '../_model/User';

@Injectable()
export class LoginService {
  private readonly ApiURL = `${environment.apiURL}/Login/`;

  constructor(private http: HttpClient) { }

  singIn(email, password): Observable<ResponseBase<User>> {
    var request = { email: email, password: password };
    var response = this.http.post<any>(this.ApiURL + 'Login', request, { "headers": this.getHeader() }) as Observable<ResponseBase<User>>;
    response.subscribe(i => {
      if (i.success) {
        this.saveLocalStorage(i.data);
      }

    });

    return response;
  }

  saveLocalStorage(data) {
    localStorage.setItem("data", JSON.stringify(data));
  }

  getLocalStorage(key) {
    var local = JSON.parse(localStorage.getItem("data"));
    return local[key];
  }

  getLocalUser() : User{
    var user = JSON.parse(localStorage.getItem("data")) as User;    
    return user;
  }


  getHeader(contentType = 'application/json'): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', contentType);
    headers = headers.set('Accept', contentType);
    headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers = headers.set('X-Requested-With', 'XMLHttpRequest');
    return headers;
  }

}


