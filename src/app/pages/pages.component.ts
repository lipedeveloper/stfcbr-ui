import { Component, OnInit } from '@angular/core'; 
import { Router } from '@angular/router';
import { User } from '../_model/User';
import { LoginService } from '../_service/login_service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {
  public RealName: string;
  public PlayerName: string;

  constructor(private router: Router, private loginService: LoginService) {
    let user = this.loginService.getLocalUser();
    this.RealName = user.realName;
    this.PlayerName = user.playerProfile.name;

  }

  ngOnInit() {
  }

  Logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
