import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { LoginService } from '../_service/login_service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  profileForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });
  

  
  constructor(private loginService: LoginService, private router: Router,private _snackBar: MatSnackBar) { }

  openSnackBar(message) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: message
    });
  }

  ngOnInit() {
  }

  
  onSubmit() {
    this.loading = true;
    this.loginService.singIn(this.profileForm.value.email, this.profileForm.value.password)
    .subscribe(
      (response) => {
        if (response.success)
          this.router.navigate(['pages']);
        else
          this.openSnackBar(response.message);

          this.loading = false;
      },
      error => (err => { this.loading = false; console.log(err) })
    );
  }

}
