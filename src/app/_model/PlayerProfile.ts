import { PowerProfile } from "./PowerProfile";

export class PlayerProfile {
    name: string;
    level: number;
    powerProfile: PowerProfile;
}
