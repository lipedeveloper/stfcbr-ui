import { Alliances } from "./Alliances";
import { PlayerProfile } from "./PlayerProfile";

export class User {
    realName: string;
    email: string;
    password: string;
    celPhone: string;
    alliances: Alliances;
    playerProfile: PlayerProfile;
    typeAccess: number; 
    // constructor(name: string, email: string, password:string  ) {
    //     this.RealName = name;
    //     this.Email = email;
    //     this.Password = password;
       
    // }
 
 
    typeAccessFn() : string{
        switch (this.typeAccess) {
            case 0:
                return "Anônimo";
            case 1:
                return "Usuário";
            case 2:
                return "Moderador";
            case 3:
                return "Administrador";
            case 4:
                return "SuperAdministrador";
                            
            default:
                return "?";
        }
    }

    validate(): boolean {
        return (this.realName != null && this.email != null && this.password != null);
    }
}
