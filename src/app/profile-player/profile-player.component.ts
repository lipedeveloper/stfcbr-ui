import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_model/User';
import { LoginService } from '../_service/login_service';

@Component({
  selector: 'app-profile-player',
  templateUrl: './profile-player.component.html',
  styleUrls: ['./profile-player.component.css']
})
export class ProfilePlayerComponent implements OnInit {
 
  public user: User;
  public typeAcessText: string;
  public imgAvatar: string;

  constructor(private router: Router, private loginService: LoginService) {
    this.user = this.loginService.getLocalUser() as User; 
    this.typeAcessText = this.typeAccessFn(this.user.typeAccess);
    this.imgAvatar = "https://13u4696j380164xc32e5slva-wpengine.netdna-ssl.com/wp-content/uploads/2021/06/avatar.png";
  }

  typeAccessFn(typeAccess:number) : string{
    switch (typeAccess) {
        case 0:
            return "Anônimo";
        case 1:
            return "Usuário";
        case 2:
            return "Moderador";
        case 3:
            return "Administrador";
        case 4:
            return "Super Administrador";
                        
        default:
            return "?";
    }
}

  ngOnInit(): void {
  }

}
