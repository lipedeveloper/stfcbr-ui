import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashBoardComponent } from './dash-board/dash-board.component';
import { PagesComponent } from './pages/pages.component';
import { ProfilePlayerComponent } from './profile-player/profile-player.component';
 


const routes: Routes = [
  {
    path: 'pages', component: PagesComponent, children: [
      { path: 'pages/', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashBoardComponent }, 
      { path: 'profile-player', component: ProfilePlayerComponent }, 

    ]
  },
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
